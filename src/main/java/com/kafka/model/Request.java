package com.kafka.model;

import java.util.ArrayList;
import java.util.List;


public class Request {
	private String tid;
	private String channel;
	private String topic;
	private List<SubData> data;
		
	public Request(String tid, String channel, String topic, List<SubData> data) {
		super();
		this.tid = tid;
		this.channel = channel;
		this.topic = topic;
		this.data = data;
	}
	
	public Request() {
		
	}

	public String getTid() {
		return tid;
	}
	
	public void setTid(String tid) {
		this.tid = tid;
	}
	
	public String getChannel() {
		return channel;
	}
	
	public void setChannel(String channel) {
		this.channel = channel;
	}
	
	public String getTopic() {
		return topic;
	}
	
	public void setTopic(String topic) {
		this.topic = topic;
	}
	
	public List<SubData> getData() {
		return data;
	}
	
	public void setData(List<SubData> data) {
		this.data = data;
	}
	
	public String toString() {
		ArrayList<String> vSubData = new ArrayList<String>();
		for (SubData sd : data) {
			vSubData.add(
			"{" +
				"name:'" + sd.getName() + '\'' +
				",value:'" + sd.getValue() + '\'' +
			"}");
		}
		return "{" +
				"tid:'" + tid + '\'' +
				",channel:'" + channel + '\'' +
				",topic:'" + topic + '\'' +	
				",data:" +
					vSubData.toString() +
				"}";
	}
}
