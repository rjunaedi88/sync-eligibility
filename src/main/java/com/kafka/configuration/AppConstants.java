package com.kafka.configuration;

public class AppConstants {
	public static final String NGVS1_TOPICNAME = "topic_ngvs1";
	public static final String NGVS1_GROUPID = "redemption";
	public static final String NGVS2_TOPICNAME = "topic_ngvs2";
	public static final String NGVS2_GROUPID = "redemption";
}
