package com.kafka.integration;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.kafka.model.Request;
import com.kafka.model.Response;

@RestController
public class MainController {

	Logger logger = LoggerFactory.getLogger(MainController.class);
	
	private MainService mainService;
	@Value("${app.kafka.topic1}")
	private String topic1;

	public MainController(MainService mainService) {
		super();
		this.mainService = mainService;
	}

	@PostMapping(path="rest/redemption/notification", consumes = "application/json", produces = "application/json")
	public Response sendMessage(@RequestBody Request req) throws InterruptedException, ExecutionException, TimeoutException {
		logger.info("[{}] Received request : {}", req.getTid(), req.toString());
		Response res = new Response();
		
		if(req.getTid() == null) {
			res.setStatus("400");
			res.setDescription("tid is Mandatory field");
			
			return res;
		}
		
		if(req.getChannel() == null) {
			res.setStatus("400");
			res.setDescription("Channel is Mandatory field");
			
			return res;
		}
		
		if(req.getTopic() == null) {
			res.setStatus("400");
			res.setDescription("Topic is Mandatory field");
			
			return res;
		}
		
		//send message to kafka topic
		mainService.send(req, topic1);
		
		res.setStatus("0");
		res.setDescription("Successfully sent to kafka");
		return res;
	}
	
}
