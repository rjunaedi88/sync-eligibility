package com.kafka.integration;

import java.io.IOException;
import java.io.StringReader;
import java.time.Duration;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.tomcat.util.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.web.client.RestTemplate;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.kafka.model.Request;
import com.kafka.model.SubData;

@Service
public class MainService {
    private static final Logger LOGGER = LoggerFactory.getLogger(MainService.class);

    private KafkaTemplate<String, Request> kafkaTemplate;
    private CGIDecoderService cgiDecoderService;
    
	@Value("${app.endpoint.mapgatewaygeneric}")
	private String mapGatewayGeneric;
	@Value("${app.mapgateway.cred.username}")
	private String mapGatewayUsername;
	@Value("${app.mapgateway.cred.password}")
	private String mapGatewayPwd;
	@Value("${app.mapgateway.connection.timeout.in.second}")
	private int mapGateWayConnectionTimeout;
	@Value("${app.mapgateway.read.timeout.in.second}")
	private int mapGateWayReadTimeout;
	@Value("${app.kafka.producer.timeout.in.ms}")
	private int kafkaProducerTimeoutInMs;
	@Value("${app.kafka.topic1}")
	private String topic1;
	@Value("${app.kafka.topic2}")
	private String topic2;

        
	public MainService(KafkaTemplate<String, Request> kafkaTemplate, CGIDecoderService cgiDecoderService) {
		super();
		this.kafkaTemplate = kafkaTemplate;
		this.cgiDecoderService = cgiDecoderService;
	}

	public void send(Request req, String topic) throws InterruptedException, ExecutionException, TimeoutException {
		
		ListenableFuture<SendResult<String, Request>> result = kafkaTemplate.send(topic, req);

		result.get(kafkaProducerTimeoutInMs, TimeUnit.MILLISECONDS);
		
		//send to kafka topic
		LOGGER.info(String.format("Message sent to topic %s with payload -> %s", topic, req));
	}
	
	//This method will consume from topic ngvs1 then continue to produce to the ngvs2 topic
	@KafkaListener(id = "ngvs1identifier",topics="#{'${app.kafka.topic1}'}", groupId="#{'${app.kafka.group.id.1}'}")
	public void listen(Request message, Acknowledgment ack) throws InterruptedException, ExecutionException, TimeoutException {
		LOGGER.info(String.format("Message received with payload -> %s", message));
		
		
		boolean cgiFounded = false;
		boolean cgiKeyFounded = false;
		boolean lacKeyFounded = false;
		boolean ciKeyFounded = false;
		
		String lac = "";
		String ci = "";
		String msisdn = "";
		List<SubData> newData = message.getData();
		
		if(message.getData().size() > 0) {
			
			for(SubData data : message.getData()) {
				if(data.getName().equals("CGI")) {
					cgiKeyFounded = true;
					if(!data.getValue().isEmpty()) {
						cgiFounded = true;
						//Decode CGI
						HashMap<String, String> decodedCGI = cgiDecoderService.decodeCGI(data.getValue());
						lac = decodedCGI.get("lac");
						ci = decodedCGI.get("ci");		
					}
				}
				
				if(data.getName().equals("LAC")) {
					lacKeyFounded = true;
				}
				
				if(data.getName().equals("Cell ID")) {
					ciKeyFounded = true;
				}
				//getting msisdn for invoking MapGatewayGeneric 
				if(data.getName().equals("Benefit MSISDN")) {
					msisdn = data.getValue();
				}
			}
			
			//This process is when cgi not attached from ngvs
			if(!cgiFounded) {
				//Call MapGatewayGeneric
				HashMap<String, String> mapGatewayResp = this.callMapGatewayGeneric(msisdn, message.getTid());
				String cgi = mapGatewayResp.get("cgi");
				lac = mapGatewayResp.get("lac");
				ci = mapGatewayResp.get("ci");
				
				if(cgiKeyFounded) {
					for(SubData data : newData) {
						if(data.getName().equals("CGI")) {
							data.setValue(cgi);
						}
					}	
				}else {
					SubData cgiObj = new SubData();
					cgiObj.setName("CGI");
					cgiObj.setValue(cgi);
					newData.add(cgiObj);
				}
							
			}
			
			if(lacKeyFounded) {
				for(SubData data : newData) {
					if(data.getName().equals("LAC")) {
						data.setValue(lac);
					}
				}	
			}else {
				SubData lacObj = new SubData();
				lacObj.setName("LAC");
				lacObj.setValue(lac);
				newData.add(lacObj);
			}
			
			if(ciKeyFounded) {
				for(SubData data : newData) {
					if(data.getName().equals("Cell ID")) {
						data.setValue(ci);
					}
				}	
			}else {
				SubData ciObj = new SubData();
				ciObj.setName("Cell ID");
				ciObj.setValue(ci);
				newData.add(ciObj);
			}
					
			message.setData(newData);
		}
				
		//send to second kafka topic
		this.send(message, topic2);	
		ack.acknowledge();

		
		//Waiting for 1 sec before next consumption
		Thread.sleep(1000);
	}
	
	public HashMap<String, String> callMapGatewayGeneric(String msisdn, String transId) {
		
		//If msisdn start with 0 then change it to 62 format
		if(msisdn.charAt(0) == '0') {
			msisdn = "62"+msisdn.substring(1);
		}else if(msisdn.charAt(0) == '8') {
			msisdn = "62"+msisdn;
		}
		String cgi = "";
		String lac = "";
		String ci = "";
				
		try {
			//Generating basic auth
			String plainCreds = mapGatewayUsername+":"+mapGatewayPwd;
			byte[] plainCredsBytes = plainCreds.getBytes();
			byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
			String base64Creds = new String(base64CredsBytes);
			
			//Invocation process
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_XML);
			headers.add("Authorization", "Basic "+base64Creds);
			
			//Generating TID
			Date now = new Date();
			long tid = now.getTime() / 1000L;
			if(transId.isEmpty()) {
				transId = Long.toString(tid);
			}
			
			String requestBody = "<locRequest>\r\n"
					+ "<tid>"+transId+"</tid>\r\n"
					+ "<msisdn>"+msisdn+"</msisdn>\r\n"
					+ "<str>EVENT</str>\r\n"
					+ "<v>1</v>\r\n"
					+ "<action>H%2780000000</action>\r\n"
					+ "<nodeid>SSP</nodeid>\r\n"
					+ "</locRequest>";
			
			HttpEntity<String> request = new HttpEntity<String>(requestBody, headers);

			RestTemplate resttemplate = new RestTemplateBuilder()
		            .setConnectTimeout(Duration.ofSeconds(mapGateWayConnectionTimeout))
		            .setReadTimeout(Duration.ofSeconds(mapGateWayReadTimeout))
		            .build();
			
			//Invoking MapGatewayGeneric Service
			LOGGER.info(String.format("Invoking MapGatewayGeneric Service : %s to : %s", request.getBody(), mapGatewayGeneric));
			ResponseEntity<String> postSelector = resttemplate.postForEntity(mapGatewayGeneric, request, String.class);
			LOGGER.info(String.format("Response status code from MapGatewayGeneric Service : %s ", postSelector.getStatusCode()));
			
			//Deserializing response
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder;
			
			builder = factory.newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(postSelector.getBody()));
			Document doc = builder.parse(is);
			
			int cgiLength = doc.getElementsByTagName("Complete").item(0).getTextContent().length();
			//Check if cgi is correct based on length criteria
			if(cgiLength == cgiDecoderService.FOUR_G_FIVE_G || cgiLength == cgiDecoderService.TWO_G_THREE_G) {
				cgi = doc.getElementsByTagName("Complete").item(0).getTextContent();
				lac = doc.getElementsByTagName("LAC").item(0).getTextContent();
				ci = doc.getElementsByTagName("CellID").item(0).getTextContent();	
			}

			
			LOGGER.info(String.format("Response body from MapGatewayGeneric Service : %s ", postSelector.getBody()));

			
		}catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		HashMap<String, String> cgiAndLacAndCi = new HashMap<String, String>();
		cgiAndLacAndCi.put("cgi", cgi);
		cgiAndLacAndCi.put("lac", lac);
		cgiAndLacAndCi.put("ci", ci);
		
		return cgiAndLacAndCi;
	}
	
	
}
