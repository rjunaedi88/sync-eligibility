package com.kafka.integration;

import java.util.HashMap;
import org.springframework.stereotype.Service;

/*
 * This service is intended to decode CGI and getting MCCMNC, LAC & CI
 */
@Service
public class CGIDecoderService {
	//this variable is for 2G or 3G CGI criteria
	final int TWO_G_THREE_G = 14;
	//this variable is for 4G or 5G CGI criteria
	final int FOUR_G_FIVE_G = 18;

	/*
	 * @return key-value pair of decoded CGI 
	 * "mmcmnc" => "xxxxx" 
	 * "lac" => "xxxx"
	 * "ci" => "xxxxx"
	 */
	public HashMap<String, String> decodeCGI(String cgi){
		HashMap<String, String> decodedCGI = new HashMap<String, String>();
		String mccmnc = "";
		String lac = "";
		String ci = "";
		
		switch(cgi.length()){
			case TWO_G_THREE_G:
				mccmnc = cgi.substring(0,6).toString();
				lac = Integer.toString(Integer.parseInt(cgi.substring(6,10),16));
				ci = Integer.toString(Integer.parseInt(cgi.substring(10,14),16));
			break;
			case FOUR_G_FIVE_G:
				mccmnc = cgi.substring(0,6).toString();
				lac = Integer.toString(Integer.parseInt(cgi.substring(6,10),16));
				ci = Integer.toString(Integer.parseInt(cgi.substring(10,18),16));
			break;
		}
		
		decodedCGI.put("mccmnc", mccmnc);
		decodedCGI.put("lac", lac);
		decodedCGI.put("ci", ci);
		
		return decodedCGI;
	}
}
