//package com.kafka.integration;
//
//
//import java.util.HashMap;
//import java.util.Map;
//
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.Test;
//import org.mockito.InjectMocks;
//import org.springframework.boot.test.context.SpringBootTest;
//
//@SpringBootTest
//public class CGIDecoderServiceTest {
//
//	@InjectMocks
//	private CGIDecoderService cgiDecoderService;
//	
//	@Test
//	void should_return_decoded_cgi() {
//		//This map contains "encoded cgi" => "decoded cgi" key-value pair as the expected 
//		//encoded and decoded cgi
//		HashMap<String, String> cgi = new HashMap<String, String>();
//		//Test cases
//		//4G/5G
//		cgi.put("15f010813704944a02", "15f010,33079,76827138");
//		cgi.put("15f01000000124ff01", "15f010,0,19201793");
//		cgi.put("15f010000001252c01", "15f010,0,19213313");
//		//2G/3G
//		cgi.put("15f01003f0e54c", "15f010,1008,58700");
//		cgi.put("15f01004533262", "15f010,1107,12898");
//		cgi.put("15f010045eb9ef", "15f010,1118,47599");
//		
//		for(Map.Entry<String, String> set : cgi.entrySet()) {
//			String mccmnc = set.getValue().split(",")[0];
//			String lac = set.getValue().split(",")[1];
//			String ci = set.getValue().split(",")[2];
//			
//			//decode cgi
//			HashMap<String, String> decodedCGI = cgiDecoderService.decodeCGI(set.getKey());
//
//			Assertions.assertEquals(mccmnc, decodedCGI.get("mccmnc"));
//			Assertions.assertEquals(lac, decodedCGI.get("lac"));
//			Assertions.assertEquals(ci, decodedCGI.get("ci"));
//		}
//		
//	}
//	
//	@Test
//	void should_return_empty_string_if_cgi_length_not_14_or_18() {
//		HashMap<String, String> cgi = new HashMap<String, String>();
//		//put wrong cgi length
//		cgi.put("15f010813704", "");
//		
//		for(Map.Entry<String, String> set : cgi.entrySet()) {
//			//decode cgi
//			HashMap<String, String> decodedCGI = cgiDecoderService.decodeCGI(set.getKey());
//			Assertions.assertEquals("", decodedCGI.get("mccmnc"));
//			Assertions.assertEquals("", decodedCGI.get("lac"));
//			Assertions.assertEquals("", decodedCGI.get("ci"));
//		}
//	}
//	
//	@Test
//	void should_return_decoded_cgi_v2() {
//		String cgi1= "15f010813704944a02";
//		String mccmnc1 = "15f010";
//		String lac1 = "33079";
//		String ci1 = "76827138";
//		
//		String cgi2= "15f01000000124ff01";
//		String mccmnc2 = "15f010";
//		String lac2 = "0";
//		String ci2 = "19201793";
//		
//		String cgi3= "15f01003f0e54c";
//		String mccmnc3 = "15f010";
//		String lac3 = "1008";
//		String ci3 = "58700";
//		
//		//decode cgi 1
//		HashMap<String, String> decodedCGI1 = cgiDecoderService.decodeCGI(cgi1);
//		Assertions.assertEquals(mccmnc1, decodedCGI1.get("mccmnc"));
//		Assertions.assertEquals(lac1, decodedCGI1.get("lac"));
//		Assertions.assertEquals(ci1, decodedCGI1.get("ci"));
//		
//		//decode cgi 2
//		HashMap<String, String> decodedCGI2 = cgiDecoderService.decodeCGI(cgi2);
//		Assertions.assertEquals(mccmnc2, decodedCGI2.get("mccmnc"));
//		Assertions.assertEquals(lac2, decodedCGI2.get("lac"));
//		Assertions.assertEquals(ci2, decodedCGI2.get("ci"));
//		
//		//decode cgi 3
//		HashMap<String, String> decodedCGI3 = cgiDecoderService.decodeCGI(cgi3);
//		Assertions.assertEquals(mccmnc3, decodedCGI3.get("mccmnc"));
//		Assertions.assertEquals(lac3, decodedCGI3.get("lac"));
//		Assertions.assertEquals(ci3, decodedCGI3.get("ci"));
//	}
//}
